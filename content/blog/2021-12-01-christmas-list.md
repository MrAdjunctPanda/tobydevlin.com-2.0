---
layout: post
date: 2021-12-01T08:00:00.000Z
title: Dear Santa
publish: false
image: /content/img/netlifyCMS/santa.png
tags: []
---

# Dear Santa,

This year I have been really good, I think, and so id love to get some sick ass Christmas presents! Here's a list of stuff that would be nice. I hope you get the internet up in Lapland else this is just very silly!

-   In-ear sports earphones Bluetooth, something like [the Bose Soundsports](https://www.currys.co.uk/gbuk/audio-and-headphones/headphones/headphones/bose-soundsport-wireless-bluetooth-headphones-aqua-10145623-pdt.html)
-   New desk matt that's around 90cm x 42cm, something like [this](https://www.orbitkey.eu/collections/orbitkey-desk-mat/products/orbitkey-desk-mat?gclid=CjwKCAiAhreNBhAYEiwAFGGKPCG97qVTn1Kpx0hg9T1lPadUsS1Lj7-Zrm9OuN8GXR9EYC_Fc5fw9xoC2BkQAvD_BwE&variant=32753874206816) or the [large, felt padded version of this](https://www.harberlondon.com/collections/desk-mat/products/leather-desk-mat?variant=1022937858060)
-   Something to inspire me to travel to a fantastic country, like a guide/storybook or an experience recommendation.
-   A funnny or supprisingly useful t-shirt.
-   A sick af alarm clock, like the [LaMetric](https://lametric.com/en-GB/time/tech-specs) or the [Timebox EVO](https://www.divoom.com/products/divoom-timebox-evo)
-   [Framed print of London](https://evermade.com/collections/jenni-sparks/products/hand-drawn-map-of-london?variant=13161709830208), with a black frame! Id Like to hng it in the sitting room for now.

This is just a small list of things I think I'd like!

Yours snowingly,

Toby
